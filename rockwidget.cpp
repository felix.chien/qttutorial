#include "rockwidget.h"
#include <QLabel>
#include <QPushButton>
#include <QMessageBox>

RockWidget::RockWidget(QWidget *parent)
    : QWidget{parent}
{
    //setWindowTitle("Rock Widget here");
    QLabel* label = new QLabel("This is a label",this);

    //Add a styled widget and move it around
    QPalette label1palette;
    label1palette.setColor(QPalette::Window, Qt::yellow);
    label1palette.setColor(QPalette::WindowText,Qt::blue);

    QFont label1font("Times", 20, QFont::Bold);

    QLabel* label1 = new QLabel(this);
    label1->setAutoFillBackground(true);
    label1->setText(("My color label"));
    label1->setFont(label1font);
    label1->setPalette(label1palette);
    label1->move(50,50);

    //Add another label
    QPalette label2palette;
    label2palette.setColor(QPalette::Window, Qt::green);
    label2palette.setColor(QPalette::WindowText, Qt::black);

    QLabel* label2 = new QLabel(this);
    label2->setAutoFillBackground(true);
    label2->setPalette(label2palette);
    label2->move(70,70);
    label2->setText("This is my label 2");
    QFont serifFont("Times", 20, QFont::Bold);
    label2->setFont(serifFont);

    //Add a button and connect to slot
    QPushButton* button = new QPushButton(this);
    button->setText("Click me");
    QFont buttonFont("Times", 30, QFont::Bold);
    button->setFont(buttonFont);
    button->move(100,250);
    connect(button,SIGNAL(clicked()), this, SLOT(buttonClicked()));
}

void RockWidget::buttonClicked()
{
    QMessageBox::information(this, "Message", "You clicked on my button");
}

QSize RockWidget::sizeHint() const
{
    return QSize(500,500);
}
